#include <iostream>
#include <fstream>
#include <openssl/ssl.h>
#include <unistd.h>
#include <netdb.h>

using namespace std;

int createSocket (string host, string port){
    int sock = -1;
    struct addrinfo * ai;
    if (getaddrinfo(host.c_str(), port.c_str(), NULL, &ai) != 0) return -1;
    if ((sock = socket(ai->ai_family, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        freeaddrinfo(ai);
        return -1;
    }
    if (connect(sock, ai->ai_addr, ai->ai_addrlen) == -1) {
        close(sock);
        freeaddrinfo(ai);
        return -1;
    }
    return sock;
}

int clean( SSL * ssl, SSL_CTX * ctx, int socket){
    if(ssl) {
        SSL_shutdown(ssl);
        SSL_free(ssl);
    }
    if(ctx) SSL_CTX_free(ctx);
    close(socket);
    return 1;
}

int main(){
    int socket = -1;
    string host = "fit.cvut.cz", port = "443";
    string getText = "GET /student/odkazy HTTP/1.0\n\n";
    string indexFileName = "index.html";
    string allCertsFileName = "/etc/ssl/certs/ca-certificates.crt";
    string certFileName = "cert.PEM";
    SSL_CTX * ctx = NULL;
    SSL * ssl = NULL;
    X509 * certificate = NULL;
    FILE * file = NULL;
    ofstream of;
    char buffer[1025];
    memset(buffer, '\0', 1025);
    if((socket = createSocket(host, port)) == -1){
        cout<<"Nelze se připojit."<<endl;
        return 1;
    }
    if(!SSL_library_init()){
        cout<<"Nepodařilo se inicializovat SSL knihovnu."<<endl;
        return clean(ssl, ctx, socket);
    }
    ctx = SSL_CTX_new(SSLv23_client_method());
    if(!SSL_CTX_load_verify_locations(ctx, allCertsFileName.c_str(), NULL)){
        cout<<"Nepodařilo se načíst adresář pro certifikát."<<endl;
        return clean(ssl, ctx, socket);
    }
    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);
    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, socket);
    SSL_set_cipher_list(ssl, "ALL:!AES");
    if (!SSL_connect(ssl)){
        cout<<"Nepodařilo se vytvořit SSL spojení."<<endl;
        return clean(ssl, ctx, socket);
    }
    long tmp = SSL_get_verify_result(ssl);
    if( tmp != X509_V_OK ){
        cout<<"Nepodařilo se ověřit certifikát. Navratový kód = "<<tmp<<"."<<endl;
        return clean(ssl, ctx, socket);
    }
    if (SSL_write(ssl, getText.c_str(), getText.length()) < (int)getText.length()){
        cout<<"Nepovedlo se poslat cely prikaz."<<endl;
        return clean(ssl, ctx, socket);
    }
    of.open(indexFileName, ofstream::out);
    while(SSL_read(ssl, buffer, 1024) > 0){
        of<<buffer<<endl;
        for ( int i = 0; i < 1025; i++) buffer[i] = '\0';
    }
    certificate = SSL_get_peer_certificate(ssl);
    if (!certificate){
        cout<<"Chyba získání certifikátu."<<endl;
    }
    file = fopen(certFileName.c_str(), "w");
    if (file == NULL){
        cout<<"Chyba otevření souboru pro certifikát."<<endl;
        return clean(ssl, ctx, socket);
    }

    if(!PEM_write_X509(file, certificate)){
        cout<<"Chyba zápisu do souboru pro certifikát."<<endl;
        return clean(ssl, ctx, socket);
    }
    cout<<SSL_CIPHER_get_name(SSL_get_current_cipher(ssl))<<endl;
    const char * cipher_name;
    int i = 0;
    cout<<"--------------------------"<<endl;
    do{
        cipher_name = SSL_get_cipher_list(ssl, i);
        if (cipher_name) cout<<cipher_name<<endl;
        i++;
    }while(cipher_name);
    clean(ssl, ctx, socket);
    return 0;
}